// By: Diego Núñez González

#include <stdio.h>
#include <pthread.h>
#include <semaphore.h>

// Se instancia el semáforo
sem_t semaforo;

// La variable global contador permite que las distintas hebras impriman numeros enteros consecutivos
int contador = 0;

// La función imprimir_entero imprime la variable contador y la incrementa
void* imprimir_entero(void* nombre_hilo) {
    char *id = (char*)nombre_hilo;

    for (int i = 0; i < 5; i++) {   
        sem_wait(&semaforo); // Se garantiza la exclusión mutua entre hebras

        if (contador % 5 == 0) printf("\n");

        // La sintaxis ++contador, incrementa el valor de la variable antes de leerla.
        // La variable empieza en 0 como se pide en la guía, e imprime desde 1 como muestra el ejemplo.
        printf("%s impresiones %d\n", id, ++contador);

        sem_post(&semaforo); // Se libera la variable semáforo para que otra hebra, o incluso la misma, puedan acceder después. Por esta misma razón, las salidas varían con cada ejecución del código
        }

    return NULL;
}

int main() {
    // Se inicializa el semáforo
    sem_init(&semaforo, 0, 1);

    // Se van a crear 5 hilos
    pthread_t hilos[5];
    char *nombres[] = {"Hilo 0", "Hilo 1", "Hilo 2", "Hilo 3", "Hilo 4"};
    
    for (int id = 0; id < 5; id++) {
        // La función pthread_create crea 5 hilos, cada uno con su nombre, que ejecutarán la función imprimir_entero
        int ret = pthread_create(&hilos[id], NULL, imprimir_entero, nombres[id]);
        if (ret) {
            printf("Error al crear %s", nombres[id]);
        }
    }

    // Antes de que el programa principal termine, debe esperar a que las hebras terminen
    for (int i = 0; i < 5; i++) {
        pthread_join(hilos[i], NULL);
    }
    
    sem_destroy(&semaforo); // Antes de terminar la ejecución, se destruye el semáforo

    return 0;
}
