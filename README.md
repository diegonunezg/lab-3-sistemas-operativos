# Lab 3 Sistemas Operativos



## Resolución del tercer laboratorio del ramo Sistemas Operativos y Redes

---
Los ejercicios son los siguientes: 

1.  Escriba un programa en C usando 5 hebras (subprocesos) donde cada una de ellas imprimirá 5 enteros. Todas las hebras ejecutan la misma función. Las salidas se verán así:

    _Hilo 0 impresiones 1_   \
    _Hilo 0 impresiones 2_   \
    _Hilo 0 impresiones 3_   \
    _Hilo 0 impresiones 4_   \
    _Hilo 0 impresiones 5_   

    _Hilo 1 impresiones 6_   \
    _Hilo 1 impresiones 7_   \
    _..._                    \
    _Hilo 4 impresiones 21_  \
    _..._                    \
    _Hilo 4 impresiones 25_  


2.	Escriba un programa en C que para una variable global “impresiones” (inicialmente igual a 0), permita de forma concurrente garantizar la exclusión mutua de 5 hebras mediante semáforos. Las salidas se verán algo así (no necesariamente es la misma salida):

    _Hilo 0 impresiones 1_   \
    _Hilo 1 impresiones 2_   \
    _Hilo 3 impresiones 3_   \
    _Hilo 2 impresiones 4_   \
    _Hilo 4 impresiones 5_   

    _Hilo 1 impresiones 6_   \
    _Hilo 2 impresiones 7_   \
    _..._                    \
    _Hilo 3 impresiones 21_  \
    _..._                    \
    _Hilo 4 impresiones 25_  

3.	Escribir programa en C con N hebras de manera que cada hebra haga una suma de números de 1 a M donde M representa un parámetro de la función de la hebra, donde cada hebra devuelva el valor de la suma respectiva. Así, el programa principal esperará que cada hebra termine para acumular su valor de retorno, y luego mostrar el resultado de la suma total. Asuma que N es una constante en su programa.

