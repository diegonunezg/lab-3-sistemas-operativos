// By: Diego Núñez González

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>


// Se define la constante N pedida. N representa la cantidad de hebras creadas.
#define N 8


// Se define la estructura Hilo_suma, que almacena los datos necesarios para el cálculo y retorno de la suma.
struct Hilo_suma {
    int id;         // id guarda el identificador del hilo que realizará la suma
    int M;          // Como se pide que la suma empieze de 1 hasta M, la estructura solo guarda el fin (M)
    int suma_final; // Se guarda el resultado de la suma desde 1 hasta M (fin)
};

typedef struct Hilo_suma Hilo_suma;


// La función suma_triangular calcula la suma desde 1 hasta el valor M almacenado en la estructura.
void* suma_triangular(void* entrada) {
    Hilo_suma *datos = (Hilo_suma*)entrada;
    
    int suma = 0; // En esta variable se almacenará la suma

    printf("La hebra %d sumará del 1 al %d\t ->", datos->id, datos->M);

    for (int i = 1; i <= datos->M; i++) {   // Se suma desde i=1 hasta M = datos->M
        suma += i;
    }

    printf("\tEl resultado es: %d\n", suma);

    datos->suma_final = suma; // Se guarda el resultado de la suma en la estructura
    
    return NULL;
}

int main() {
    // Semilla para la generación de números pseudoaleatoreos. 
    // Los valores generados seran los valores M hasta los que debe sumar cada hebra.
    srand(time(NULL));

    // Se van a crear N hebras.
    pthread_t hebras[N];
    Hilo_suma datos_hebra[N];

    // Se le asignan los valores a cada una de las estructuras para la suma.
    for (int i = 0; i < N; i++) {
        datos_hebra[i].id = i+1;
        datos_hebra[i].M = (rand() % 15) + 5; // El M de cada suma es un número aleatoreo entre 5 y 19. Se pueden cambiar estos valores si se desea
        datos_hebra[i].suma_final = 0;
    }

    // Se crean las hebras
    for (int i = 0; i < N; i++) {
        // La función pthread_create crea N hebras, cada una con su nombre, que ejecutarán la función suma_triangular
        int ret = pthread_create(&hebras[i], NULL, suma_triangular, &datos_hebra[i]);
        if (ret) {
            printf("Error al crear %d", datos_hebra[i].id);
        }
        pthread_join(hebras[i], NULL); // El programa espera a que cada hebra termine, como se pide en el lab

    }

    // Como ya todas las hebras terminaron su ejecución, se calcula la suma total.
    int suma_total = 0;
    for (int i = 0; i < N; i++) {
        suma_total += datos_hebra[i].suma_final; // Se suma el valor final de cada hebra
    }
    
    printf("\n\tLa suma total de las %d hebras es: %d\n\n", N, suma_total);

    return 0;
}