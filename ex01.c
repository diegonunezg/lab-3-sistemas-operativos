// By: Diego Núñez González

#include <stdio.h>
#include <pthread.h>


// La variable global contador permite que las distintas hebras impriman numeros enteros consecutivos
int contador = 0;

// La función imprimir_entero imprime la variable contador y la incrementa 5 veces
void* imprimir_entero(void* nombre_hilo) {
    char *id = (char*)nombre_hilo;

    for (int i = 0; i < 5; i++) {
        printf("%s impresiones %d\n", id, ++contador); // ++contador incrementa la variable antes de leerla. Esto ya que la variable empieza en 0, pero en el ejemplo se pide que parta imprimiendo 1.
    }
    
    printf("\n");

    return NULL;
}

int main() {

    // Se van a crear 5 hilos
    pthread_t hilos[5];
    char *nombres[] = {"Hilo 0", "Hilo 1", "Hilo 2", "Hilo 3", "Hilo 4"};
    
    for (int id = 0; id < 5; id++) {
        // La función pthread_create crea 5 hilos, cada uno con su nombre, que ejecutarán la función imprimir_entero
        int ret = pthread_create(&hilos[id], NULL, imprimir_entero, nombres[id]);
        if (ret) {
            printf("Error al crear %s", nombres[id]);
        }
        pthread_join(hilos[id], NULL); // Se espera a que los hilos terminen
    }

    return 0;
}
